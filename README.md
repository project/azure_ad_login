# Azure AD login

Fill the form in admin/config/services/azure-ad-login with
Azure AD login credentiasl.

Also, when the Azure AD login is built in Azure Devops some roles must be create
to macth with Roles created in Drupal.

Drupal module callback for Azure Devops configuration is `callback_azure_ad`,
example : `https://domain.com/callback_azure_ad`
