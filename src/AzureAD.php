<?php

namespace Drupal\azure_ad_login;

use Drupal\Core\Url;
use Drupal\Core\Link;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Azure Active record connection class.
 */
class AzureAD {
  use StringTranslationTrait;

  protected const AUTORIZATIONENDPOINT = 'oauth2/v2.0/authorize';

  protected const TOKENENDPOINT = 'oauth2/v2.0/token';

  protected const CALLBACK = 'callback_azure_ad';

  /**
   * Azure active directory settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $azureADSettings;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   */
  public function __construct(ConfigFactoryInterface $config, Client $http_client, LoggerChannelFactoryInterface $logger_factory, Request $request) {
    $this->azureADSettings = $config->get('azure_ad_login.settings');
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Build login url for login form.
   *
   * @return string
   *   Return a url to connect with Azure.
   */
  public function loginUrl(): ?string {

    if (count($this->drupalRoles()) == 0) {
      return NULL;
    }

    $this->redirectUrl();

    $url = Url::fromUri($this->loginUrlPath(), $this->loginUrlOptions());
    $text = $this->t('Login with your Azure AD account');
    $html_link = Link::fromTextAndUrl($text, $url);

    return $html_link->toString();
  }

  /**
   * Get the token with the code from the first oauth call.
   *
   * @var string $code
   *
   * @return array
   *   Return azure token.
   */
  public function getToken(string $code):array {
    $url = Url::fromUri($this->tokenUrlPath());
    $options = $this->tokenUrlOptions($code);

    try {
      $response = $this->httpClient->request('POST', $url->toString(), $options);
      if ($response->getStatusCode() === 200) {

        $token_data = json_decode((string) $response->getBody(), TRUE);
        return $token_data;
      }
    }
    catch (RequestException $e) {
      $this->errorLog('Could not retrieve tokens', $e->getMessage());
    }
    return [];
  }

  /**
   * Load Azure profile.
   *
   * @var string $access_token
   *
   * @return array
   *   Array with Azure user profile.
   */
  public function loadUserProfile(string $access_token): array {

    try {
      $azure_profile = $this->httpClient->request(
        'GET',
        $this->azureADSettings->get('graph_path') . '/v1.0/me',
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $access_token,
            'Content-Type' => 'application/json',
          ],
        ],
      );

      $user_profile = json_decode((string) $azure_profile->getBody(), TRUE);
      return $user_profile;
    }
    catch (RequestException $e) {
      $this->errorLog('Could not retrieve Azure profile', $e->getMessage());
    }
    return [];
  }

  /**
   * Retrieve the list of user's group.
   *
   * @var string $user_id
   * @var string $token
   *
   * @return array
   *   Return the list of groups.
   */
  public function userGroupList(string $user_id, string $access_token): array {

    try {
      // $body = json_encode(['securityEnabledOnly' => 'FALSE']);
      $azure_groups = $this->httpClient->request(
        'GET',
        $this->azureADSettings->get('graph_path') . '/v1.0/users/' . $user_id . '/memberOf?$select=id,displayName',
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $access_token,
          ],
        ],
      );

      $groups = json_decode((string) $azure_groups->getBody(), TRUE);
      foreach ($groups['value'] as $group) {
        if ($group['@odata.type'] === '#microsoft.graph.group') {
          $group_list[] = $group['displayName'];
        }
      }
      return $group_list;
    }
    catch (RequestException $e) {
      $this->errorLog('Could not retrieve Azure user groups', $e->getMessage());
    }

  }

  /**
   * Decode id token information.
   *
   * @var array $token_data
   *
   * @return array
   *   return an array with the code.
   */
  public function decodeIdToken(array $token_data): array {
    [, $claims64] = explode('.', $token_data['id_token']);
    $claims64 = str_replace(['-', '_'], ['+', '/'], $claims64);
    $claims64 = base64_decode($claims64);
    $decoded = json_decode($claims64, TRUE);
    return $decoded;
  }

  /**
   * Return the list of roles configure in drupal.
   *
   * @return array
   *   array with all the groups that are selected to compare with Azure groups.
   */
  public function drupalRoles(): array {
    $role_list = $this->azureADSettings->get('role_group_map');
    $list = [];
    foreach ($role_list as $value) {
      if ($value !== 0) {
        $list[] = $value;
      }
    }
    return $list;
  }

  /**
   * Build basic login path.
   *
   * @return string
   *   return Autorizatoiin end point path.
   */
  private function loginUrlPath():string {
    return $this->azureADSettings->get('autorize_endpoint') .
      '/' .
      $this->azureADSettings->get('tennat') .
      '/' .
      self::AUTORIZATIONENDPOINT;
  }

  /**
   * Build url login options.
   *
   * @return array
   *   Return array of opitons for login url.
   */
  private function loginUrlOptions():array {
    return [
      'query' => [
        'client_id' => $this->azureADSettings->get('client_id'),
        'response_type' => 'code',
        'scope' => 'openid email profile',
        'redirect_uri' => $this->redirectUrl(),
      ],
    ];
  }

  /**
   * Build the url to ask the token.
   *
   * @return string
   *   Return basic url path.
   */
  private function tokenUrlPath():string {
    return $this->azureADSettings->get('token_endpoint') .
      '/' .
      $this->azureADSettings->get('tennat') .
      '/' .
      self::TOKENENDPOINT;
  }

  /**
   * Return options form url token.
   *
   * @var string $code
   *
   * @return array
   *   Options in an array to build post.
   */
  private function tokenUrlOptions(string $code):array {
    return [
      'form_params' => [
        'code' => $code,
        'client_id' => $this->azureADSettings->get('client_id'),
        'client_secret' => $this->azureADSettings->get('secret_value'),
        'redirect_uri' => $this->redirectUrl(),
        'grant_type' => 'authorization_code',
      ],
    ];
  }

  /**
   * Return callback function path.
   *
   * @return string
   *   the callback path
   */
  private function redirectUrl():string {
    return $this->request->getSchemeAndHttpHost() . '/' . $this::CALLBACK;
  }

  /**
   * Function to log errors.
   *
   * @var string $message
   * @var string $error
   */
  private function errorLog(string $message, string $error) {
    $variables = [
      '@message' => $message,
      '@error_message' => $error,
    ];
    $this->loggerFactory->get('Azure_ad_login')->error('@message. Details: @error_message', $variables);
  }

}
