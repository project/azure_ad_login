<?php

namespace Drupal\azure_ad_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Azure AD Login settings for this site.
 */
class AzureADSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'azure_ad_login_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['azure_ad_login.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('azure_ad_login.settings');
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cliente ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['secret_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret value'),
      '#default_value' => $config->get('secret_value'),
      '#required' => TRUE,
    ];

    $form['tennat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('tennat'),
      '#default_value' => $config->get('tennat'),
      '#required' => TRUE,
    ];

    $form['app_registration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App registration'),
      '#default_value' => $config->get('app_registration'),
      '#required' => TRUE,
    ];

    $form['autorize_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autorize endpoint'),
      '#default_value' => $config->get('autorize_endpoint'),
      '#required' => TRUE,
      '#description' => $this->t('Add autorize endpoint first part of url: https://login.microsoftonline.com .'),
    ];

    $form['token_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token endpoint'),
      '#default_value' => $config->get('token_endpoint'),
      '#required' => TRUE,
      '#description' => $this->t('Add token endpoint first part of url: https://login.microsoftonline.com .'),
    ];

    $form['graph_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Graph ql path.'),
      '#default_value' => $config->get('graph_path'),
      '#required' => TRUE,
      '#description' => $this->t('Add graph ql first part of url: https://graph.microsoft.com .'),
    ];

    $user_role_names = user_role_names();
    $form['role_group_map'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('List of roles to map.'),
      '#description' => $this->t('You should have the same name in Azure AD and Drupal.'),
      '#options' => $user_role_names,
      '#default_value' => $config->get('role_group_map'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $last_character = function ($path) {
      $last_character = substr($path, -1);
      return ($last_character === '/') ? FALSE : TRUE;
    };

    $validate_url = function ($path) {
      return filter_var(trim($path), FILTER_VALIDATE_URL);
    };

    $fields = ['autorize_endpoint', 'token_endpoint', 'graph_path'];

    foreach ($fields as $field) {
      if (
        $last_character(trim($form_state->getValue($field))) === FALSE ||
        $validate_url(trim($form_state->getValue($field))) === FALSE
      ) {
        $form_state->setErrorByName(
            $field,
            $this->t("The url it's wrong, please don't use / at the end and check that the url is valid.")
          );
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('azure_ad_login.settings');
    $config->set('client_id', trim($form_state->getValue('client_id')))
      ->set('secret_value', trim($form_state->getValue('secret_value')))
      ->set('tennat', trim($form_state->getValue('tennat')))
      ->set('app_registration', trim($form_state->getValue('app_registration')))
      ->set('role_group_map', $form_state->getValue('role_group_map'))
      ->set('token_endpoint', trim($form_state->getValue('token_endpoint')))
      ->set('autorize_endpoint', trim($form_state->getValue('autorize_endpoint')))
      ->set('graph_path', trim($form_state->getValue('graph_path')))
      ->save();
  }

}
