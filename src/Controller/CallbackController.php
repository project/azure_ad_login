<?php

namespace Drupal\azure_ad_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\Entity\User;
use Drupal\azure_ad_login\AzureAD;
use Drupal\Core\Language\languageManager;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Returns responses for Azure AD Login routes.
 */
class CallbackController extends ControllerBase {

  /**
   * Azure active directory settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $azureADSettings;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Password generator service.
   *
   * @var \Drupal\Core\Password\DefaultPasswordGenerator
   */
  protected $passwordGenerator;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Azure Active record connection.
   *
   * @var \Drupal\azure_ad_login\AzureAD
   */
  protected $azureAD;

  /**
   * The Language Manager Service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   */
  public function __construct(ConfigFactoryInterface $config, LoggerChannelFactoryInterface $logger_factory, PasswordGeneratorInterface $password_generator, MessengerInterface $messenger, AzureAD $azure_ad, languageManager $language) {
    $this->azureADSettings = $config->get('azure_ad_login.settings');
    $this->loggerFactory = $logger_factory;
    $this->passwordGenerator = $password_generator;
    $this->messenger = $messenger;
    $this->azureAD = $azure_ad;
    $this->languageManager = $language;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('password_generator'),
      $container->get('messenger'),
      $container->get('azure_ad_login.authentication'),
      $container->get('language_manager'),

    );
  }

  /**
   * Builds the response.
   */
  public function build(Request $request) {

    $message_error = $this->t('Something go wrong with Azure Login, ask to adminitrator', ['error']);

    $code = $request->get('code');
    if (!isset($code)) {
      $this->messenger->addError($message_error);
      $this->loggerFactory->get('Azure login')->error('The request don\'t get Azure Code');
      return $this->redirect('user.login');
    }

    $token = $this->azureAD->getToken($code);
    if (!isset($token['access_token'])) {
      $this->messenger->addError($message_error);
      $this->loggerFactory->get('Azure login')->error('The request don\'t get Azure Token');
      return $this->redirect('user.login');
    }

    $azure_profile = $this->azureAD->loadUserProfile($token['access_token']);
    if (!isset($azure_profile['id'])) {
      $this->messenger->addError($message_error);
      $this->loggerFactory->get('Azure login')->error('The request don\'t get Azure Profile');
      return $this->redirect('user.login');
    }

    // Check that the user exist or not in drupal.
    $drupal_user = user_load_by_mail($azure_profile['userPrincipalName']);

    // If the user is not in Drupal.
    if (empty($drupal_user) || !isset($drupal_user)) {
      // Get Azure user groups.
      $azure_user_group_list = $this->azureAD->userGroupList($azure_profile['id'], $token['access_token']);

      // Load the list of groups to use.
      $drupal_role_list = $this->azureAD->drupalRoles();

      // Get the roles to asing to the user.
      if (!is_array($azure_user_group_list) || !is_array($drupal_role_list)) {
        $this->messenger->addError($message_error);
        $this->loggerFactory->get('Azure login')->error('The are problems with roles.');
        return $this->redirect('user.login');
      }

      $roles = $this->roleList($azure_user_group_list, $drupal_role_list);
      $drupal_user = $this->createUser($azure_profile, $roles);
    }

    user_login_finalize($drupal_user);
    return $this->redirect('user.page');
  }

  /**
   * Functiin createUser create a new user and save in Drupal ddbb.
   *
   * @var array $account
   * @var array $roles
   *
   * @return object
   *   User object
   */
  private function createUser(array $account, array $roles):object {

    $name = $account['displayName'] ?? $account['givenName'] . ' ' . $account['surname'];
    $language = $this->languageManager->getCurrentLanguage()->getId();

    $new_user = User::create();
    $new_user->setPassword($this->passwordGenerator->generate(25));
    $new_user->setEmail($account['userPrincipalName']);
    $new_user->setUsername($name);
    $new_user->set('init', 'email');
    $new_user->enforceIsNew();
    $new_user->set('langcode', $language);
    $new_user->set('preferred_langcode', $language);
    $new_user->set('preferred_admin_langcode', $language);

    foreach ($roles as $role) {
      $new_user->addRole($role);
    }
    $new_user->activate();
    $new_user->save();
    return $new_user;
  }

  /**
   * Function to get the group/roles to assign to a user.
   *
   * @var array $azure_group
   * @var array $role_list
   *
   * @return array
   *   List of groups/roles to assign to user or empty array.
   */
  private function roleList(?array $azure_group, array $role_list): ?array {
    return array_intersect(
      array_map('strtolower', $azure_group),
      array_map('strtolower', $role_list),
    );
  }

}
